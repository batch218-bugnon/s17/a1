// console.log("Hello World");

/*
	[FUNCTION W/ PROMPT]
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function promptMessage (){
		let fullName = prompt("Enter your Full Name"); 
		let age = prompt("Enter your age");
		let location = prompt("Enter your location")

		console.log("Hello, " + fullName);
		console.log("You are " + age + " years old");
		console.log("You live in " + location);
	}

	promptMessage(); 
/*
	[FUNCTION TO DISPLAY]
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function getFavArtists (){
		console.log("1. The Beattles");
		console.log("2. Metallica");
		console.log("3. The Eagles");
		console.log("4. L 'arc~en~Ciel");
		console.log("5. Eraserheads")

	}

	getFavArtists();
/*
	[FUNCTION TO DISPLAY]
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function getFavMovies (){
		let favMovie1 = ("The God Father");
		let favMovie1Rating = ("97%");
		let favMovie2 = ("The God Father, Part II");
		let favMovie2Rating = ("96%");
		let favMovie3 = ("Shawshank Redemption");
		let favMovie3Rating = ("91%");
		let favMovie4 = ("4. To Kill a Mockingbird");
		let favMovie4Rating = ("93%");
		let favMovie5 = ("5. Psycho");
		let favMovie5Rating = ("96%");

		console.log("1. " + favMovie1);
		console.log("Rotten Tomatoes Rating: " + favMovie1Rating)
		console.log("2. " + favMovie2);
		console.log("Rotten Tomatoes Rating: " + favMovie2Rating)
		console.log("3. " + favMovie3);
		console.log("Rotten Tomatoes Rating: " + favMovie3Rating)
		console.log("4. " + favMovie4);
		console.log("Rotten Tomatoes Rating: " + favMovie4Rating)
		console.log("5. " + favMovie5);
			console.log("Rotten Tomatoes Rating: " + favMovie5Rating)
		
	}

	getFavMovies();
/*	
	[DEBUG]
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");	
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};



printFriends();
